import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import App from './App.vue'
import store from './vuex/store.js'
import Dashboard from './components/Dashboard.vue'

Vue.use(VueResource)
Vue.use(VueRouter)

Vue.http.options.credentials = true

const routes = [
  { 
    path: '/',
    component: Dashboard
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
