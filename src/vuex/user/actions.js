import Vue from 'vue'
import { handleError } from '../errorHandler'

const login = (context, payload) => {
  let success = (response) => {
    context.commit('UPDATE_USER', response.body)
    context.commit('TOGGLE_VISIBLE', true)
    context.commit('UPDATE_ERROR', { visible: false, message: '' })
  }

  Vue.http.post('http://localhost:3000/api/login', payload)
    .then(success, (err) => {
      handleError(err, context)
    })
}

const signup = (context, payload) => {
  let success = (response) => {
    context.commit('UPDATE_USER', response.body)
    context.commit('TOGGLE_VISIBLE', true)
    context.commit('UPDATE_ERROR', { visible: false, message: '' })
  }

  Vue.http.post('http://localhost:3000/api/signup', payload)
    .then(success, (err) => {
      handleError(err, context)
    })
}

const logout = (context) => {
  let success = (response) => {
    context.commit('TOGGLE_VISIBLE', false)
    context.commit('UPDATE_USER', {})
    context.commit('CLEAR_TIMER')
    context.commit('UPDATE_ERROR', { visible: false, message: '' })
  }

  Vue.http.get('http://localhost:3000/api/logout')
    .then(success, (err) => {
      handleError(err, context)
    })
}

const checkSession = (context) => {
  let success = (response) => {
    context.commit('UPDATE_USER', response.body)
    context.commit('TOGGLE_VISIBLE', true)
  }

  Vue.http.get('http://localhost:3000/api/checkSession')
    .then(success, (err) => {
      handleError(err, context)
    })
}

export default {
  login,
  signup,
  logout,
  checkSession
}