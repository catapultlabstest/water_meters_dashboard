export default {
  user(state) {
    return state.user
  },
  pageVisible(state) {
    return state.pageVisible
  }
}