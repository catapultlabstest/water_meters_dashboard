export default {
  UPDATE_USER(state, user) {
    state.user = user
  },
  TOGGLE_VISIBLE(state, visible) {
    state.pageVisible = visible
  }
}