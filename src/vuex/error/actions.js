export default {
  closeAlert(context) {
    context.commit('UPDATE_ERROR', { visible: false, message: '' })
  }
}