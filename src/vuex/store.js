import Vue from 'vue'
import Vuex from 'vuex'
import userGetters from './user/getters'
import intervalGetters from './interval/getters'
import dashboardGetters from './dashboard/getters'
import errorGetters from './error/getters'

import userMutations from './user/mutations'
import intervalMutations from './interval/mutations'
import dashboardMutations from './dashboard/mutations'
import errorMutations from './error/mutations'

import userActions from './user/actions'
import intervalActions from './interval/actions'
import dashboardActions from './dashboard/actions'
import errorActions from './error/actions'

Vue.use(Vuex)

const defaultState = {
  user: {},
  pageVisible: false,
  error: {
    visible: false,
    message: ''
  },
  refreshInterval: 30, //seconds
  timer: undefined,
  meterReadings: {
    combined: {},
    byMeters: {}
  }
}

export default new Vuex.Store({
  state: defaultState,
  getters: Object.assign(userGetters, intervalGetters, dashboardGetters, errorGetters),
  mutations: Object.assign(userMutations, intervalMutations, dashboardMutations, errorMutations),
  actions: Object.assign(userActions, intervalActions, dashboardActions, errorActions)
})