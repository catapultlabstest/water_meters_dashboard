import Vue from 'vue'
import { handleError } from '../errorHandler'

const fetchCombinedMeterReadings = (context) => {
  let success = (response) => {
    context.commit('UPDATE_COMBINED_READINGS', response.body)
  }

  Vue.http.get('http://localhost:3000/api/dashboard/combinedReadings')
    .then(success, (err) => {
      handleError(err, context)
    })
}

const fetchReadingsGroupedByMeters = (context, payload) => {
  let success = (response) => {
    context.commit('UPDATE_READINGS_GROUPED_BY_METERS', response.body)
  }

  Vue.http.post('http://localhost:3000/api/dashboard/groupedReadingsByMeters', payload)
    .then(success, (err) => {
      handleError(err, context)
    })
}

export default {
  fetchCombinedMeterReadings,
  fetchReadingsGroupedByMeters
}