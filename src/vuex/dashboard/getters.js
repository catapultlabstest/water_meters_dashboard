export default { 
  combinedMeterReadings(state) {
    return state.meterReadings.combined
  },
  readingsByMeters(state) {
    return state.meterReadings.byMeters
  }
}