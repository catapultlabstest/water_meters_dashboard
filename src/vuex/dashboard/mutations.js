export default {
  UPDATE_COMBINED_READINGS(state, payload) {
    let result = {
      categories: [],
      readings: [{ name: 'Reading', data: [] }],
      chartName: 'Combined Readings by days',
      yAxisName: 'Consumption cubic meters',
      chartType: 'line'
    }

    for (let i = 0; i < payload.length; i++) {
      result.categories.push(payload[i].date)
      result.readings[0].data.push( payload[i].reading)
    }

    state.meterReadings.combined = result
  },
  UPDATE_READINGS_GROUPED_BY_METERS(state, payload) {
    let result = {
      categories: [],
      readings: [{ name: 'Reading', data: [] }],
      chartName: 'Combined readings by meters for specified period',
      yAxisName: 'Consumption cubic meters',
      chartType: 'column'
    }

    for (let i = 0; i < payload.length; i++) {
      result.categories.push(payload[i].contact.endpointId)
      result.readings[0].data.push( payload[i].reading)
    }

    state.meterReadings.byMeters = result
  }
}