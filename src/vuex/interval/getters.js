export default { 
  refreshInterval(state) {
    return state.refreshInterval
  },
  timer(state) {
  	return state.timer
  }
}