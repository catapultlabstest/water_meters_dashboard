export default {
  UPDATE_INTERVAL(state, interval) {
    state.refreshInterval = interval
  },
  SET_TIMER(state, timer) {
    state.timer = timer
  },
  CLEAR_TIMER(state) {
    if (state.timer) return clearInterval(state.timer) 
    state.timer = undefined
  }
}