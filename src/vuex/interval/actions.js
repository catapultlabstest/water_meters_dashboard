export default {
  changeInterval(context, newInterval) {
    context.commit('UPDATE_INTERVAL', newInterval)
  },
  setTimer(context, timer) {
  	context.commit('SET_TIMER', timer)
  },
  clearTimer(context) {
  	context.commit('CLEAR_TIMER')
  }
}