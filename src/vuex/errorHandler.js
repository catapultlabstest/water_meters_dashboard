export const handleError = (response, context) => {
  if (response.status == 401) {
      context.commit('CLEAR_TIMER')
      context.commit('TOGGLE_VISIBLE', false)
  }
  
  context.commit('UPDATE_ERROR', { visible: true, message: response.body.message })
}